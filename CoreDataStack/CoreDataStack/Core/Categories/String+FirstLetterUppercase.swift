//
//  String+FirstLetterUppercase.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 28.06.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

extension String {
    internal var firstLetterUppercased: String {
        guard let first = self.first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
}
