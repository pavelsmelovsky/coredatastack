//
//  NSManagedObject+Base.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 27.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObject {
    @objc open func didImport(_ object: Any) {

    }
}
