//
//  NSManagedObject+Context.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 09.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

public extension NSManagedObject {
    func inContext(_ context: NSManagedObjectContext) -> Self? {
        if self.objectID.isTemporaryID {
            return nil
        }

        do {
            return try context.existingObject(self)
        } catch let error as NSError {
            print("Cannot fetch existing object with objectID: \(self.objectID) with error: \(error.localizedDescription)")
        }
        return nil
    }
}
