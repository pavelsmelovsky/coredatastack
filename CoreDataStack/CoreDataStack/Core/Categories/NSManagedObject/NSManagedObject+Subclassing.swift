//
//  NSManagedObject+Subclassing.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 15.12.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

public protocol NSManagedObjectSubclass where Self: NSManagedObject {
//    static var entityName: String { get }
}

extension NSManagedObject {
    @objc open class var entityName: String {
        fatalError("You must override property 'entityName' in your class \(NSStringFromClass(self))")
    }
}
