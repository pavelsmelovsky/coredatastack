//
//  NSManagedObject+Count.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 11.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

import CoreData

public extension NSManagedObject {
    static func count(predicate: NSPredicate? = nil, in context: NSManagedObjectContext? = nil) -> UInt {
        var res: Int = 0

        guard let moc = context ?? CoreDataStack.default?.viewContext else {
            contextFailure()
        }

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.entityName)
        fetchRequest.predicate = predicate

        moc.performAndWait {
            do {
                res = try moc.count(for: fetchRequest)
            } catch let error as NSError {
                #if DEBUG
                    preconditionFailure("Cannot get count of entities with error: \(error.localizedDescription)")
                #endif
            }
        }

        return UInt(res)
    }
}
