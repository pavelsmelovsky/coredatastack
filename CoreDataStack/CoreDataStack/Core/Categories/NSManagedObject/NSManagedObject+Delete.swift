//
//  NSManagedObject+Delete.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 11.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

public extension NSManagedObject {
    static func deleteAll(predicate: NSPredicate, in context: NSManagedObjectContext) {
        _deleteAll(predicate: predicate, in: context)
    }

    static func truncateAll(in context: NSManagedObjectContext) {
        _deleteAll(predicate: nil, in: context)
    }

    private static func _deleteAll(predicate: NSPredicate?, in context: NSManagedObjectContext) {
        let request = NSFetchRequest<NSManagedObject>(entityName: self.entityName)
        request.returnsObjectsAsFaults = true
        request.includesPropertyValues = false
        request.predicate = predicate

        context.performAndWait {
            do {
                let objects = try context.fetch(request)

                for object in objects {
                    context.delete(object)
                }
            } catch let error as NSError {
                print("Cannot fetch objects with error: \(error.localizedDescription)")
            }
        }
    }
}
