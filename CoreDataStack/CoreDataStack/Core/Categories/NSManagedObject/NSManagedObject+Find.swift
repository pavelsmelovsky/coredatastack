//
//  NSManagedObject+Find.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 09.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

public extension NSManagedObject {
    static func findFirst(predicate: NSPredicate? = nil, in context: NSManagedObjectContext? = nil) -> Self? {
        guard let context = context ?? CoreDataStack.default?.viewContext else {
            contextFailure()
        }

        return context.performAndReturn {
            return context.findFirst(of: self, predicate: predicate)
        }
    }

    static func find(attribute: String, isEqualTo value: Any, in context: NSManagedObjectContext? = nil) -> Self? {
        guard let context = context ?? CoreDataStack.default?.viewContext else {
            contextFailure()
        }

        return context.performAndReturn {
            return context.findFirst(of: self, where: attribute, isEqualTo: value)
        }
    }

    static func findAll(predicate: NSPredicate? = nil, in context: NSManagedObjectContext?) -> [NSManagedObject] {
        guard let context = context ?? CoreDataStack.default?.viewContext else {
            contextFailure()
        }

        return context.performAndReturn {
            return context.findAll(of: self, with: predicate)
        }
    }
}
