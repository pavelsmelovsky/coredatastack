//
//  NSPersistentStoreDescription+Init.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 13.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

extension NSPersistentStoreDescription {
    static func sql(at storeUrl: URL) -> NSPersistentStoreDescription {
        let storeDescription = NSPersistentStoreDescription(url: storeUrl)
        storeDescription.type = NSSQLiteStoreType
        storeDescription.setOption(NSNumber(value: true), forKey: NSMigratePersistentStoresAutomaticallyOption)
        storeDescription.setOption(NSNumber(value: true), forKey: NSInferMappingModelAutomaticallyOption)
        return storeDescription
    }
}
