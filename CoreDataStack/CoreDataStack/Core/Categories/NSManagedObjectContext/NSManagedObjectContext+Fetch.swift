//
//  NSManagedObjectContext+Fetch.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 09.12.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

public extension NSManagedObjectContext {
    func fetchRequest<T: NSManagedObject>(for type: T.Type, predicate: NSPredicate? = nil, sortedBy: [Sort]? = nil, page: Page? = nil) -> NSFetchRequest<T> {
        let fetchRequest = NSFetchRequest<T>(entityName: type.entityName)
        fetchRequest.predicate = predicate

        if let sortedBy = sortedBy {
            fetchRequest.sortDescriptors = self.parse(sortItems: sortedBy)
        }

        if let page = page {
            fetchRequest.fetchOffset = page.offset
            fetchRequest.fetchLimit = page.limit
        }

        return fetchRequest
    }

    private func parse(sortItems: [Sort]) -> [NSSortDescriptor] {
        if sortItems.isEmpty {
            return []
        }

        return sortItems.map({NSSortDescriptor(key: $0.keyPath, ascending: $0.ascending)})
    }
}
