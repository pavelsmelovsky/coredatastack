//
//  NSManagedObjectContext+Objects.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 10.12.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

public extension NSManagedObjectContext {
    func existingObject<T: NSManagedObject>(of type: T.Type, with objectId: NSManagedObjectID) throws -> T {
        guard let obj = try self.existingObject(with: objectId) as? T else {
            fatalError()
        }
        return obj
    }

    func existingObject<T: NSManagedObject>(_ object: T) throws -> T {
        return try self.existingObject(of: T.self, with: object.objectID)
    }
}
