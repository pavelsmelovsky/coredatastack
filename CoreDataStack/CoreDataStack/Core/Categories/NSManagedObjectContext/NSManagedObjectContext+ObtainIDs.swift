//
//  NSManagedObjectContext+ObtainIDs.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 30.12.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObjectContext {
    func obtainPermanentIds() {
        self.observeNotification(forName: .NSManagedObjectContextWillSave) { [weak self] (notification) in
            self?.obtainPermanentIds(from: notification)
        }
    }

    private func obtainPermanentIds(from notification: Notification) {
        guard let ctx = notification.object as? NSManagedObjectContext else {
            return
        }

        let insertedObjects = ctx.insertedObjects
        if !insertedObjects.isEmpty {
            try? ctx.obtainPermanentIDs(for: [NSManagedObject](insertedObjects))
        }
    }
}
