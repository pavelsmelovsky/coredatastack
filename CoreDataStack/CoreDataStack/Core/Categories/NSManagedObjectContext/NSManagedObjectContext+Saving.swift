//
//  NSManagedObjectContext+Saving.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 11.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

public extension NSManagedObjectContext {
    /// Save context to backed persistent store
    /// Important: call this method in perform or performAndWait blocks on same context
    ///
    /// - Throws: Error if any errors occured
    func saveToPersistentStoreAndWait() throws {
        if !self.hasChanges { return }

        try self.performAndWaitOrThrow {
            debugPrint("Will save context \(self.name ?? "<unknown>")")
            try self.save()
            debugPrint("Did save context \(self.name ?? "<unknown>")")

            if let parent = self.parent {
                try parent.saveToPersistentStoreAndWait()
            }
        }
    }

    func saveToPersistentStore(completion: ((Bool, NSError?) -> Void)? = nil) {
        if !self.hasChanges {
            completion?(false, nil); return
        }

        debugPrint("Save context \(self.name ?? "<nonamed>") asynchroneously")

        let saveBlock = { [weak self] in
            do {
                try self?.save()

                if let parent = self?.parent {
                    parent.saveToPersistentStore(completion: completion)
                } else {
                    completion?(true, nil)
                }
            } catch let error as NSError {
                print("Cannot save context to store: \(error.localizedDescription)")

                completion?(false, error)
            }
        }

        self.perform(saveBlock)
    }

    func performAndReturn<T>(_ block: () -> T) -> T {
        var result: T!
        self.performAndWait {
            result = block()
        }
        return result
    }

    func performAndWaitOrThrow(_ block: () throws -> Swift.Void) throws {
        var error: Error?
        self.performAndWait {
            do {
                try block()
            } catch let err {
                error = err
            }
        }

        if let error = error {
            throw error
        }
    }
}
