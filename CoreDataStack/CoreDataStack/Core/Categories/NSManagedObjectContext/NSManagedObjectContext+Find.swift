//
//  NSManagedObjectContext+Find.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 09.12.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

public extension NSManagedObjectContext {
    // MARK: - Find all

    func findAll<T: NSManagedObject>(of type: T.Type, with predicate: NSPredicate? = nil, sortedBy: [Sort]? = nil, page: Page? = nil) -> [T] {
        let fetchRequest = self.fetchRequest(for: type, predicate: predicate, sortedBy: sortedBy, page: page)
        return self.findAll(with: fetchRequest)
    }

    func findAll<T>(with request: NSFetchRequest<T>) -> [T] {
        do {
            return try self.fetch(request)
        } catch let error {
            return fatalIfDebug(error: error, orReturn: [])
        }
    }

    // MARK: - Find first

    func findFirst<T: NSManagedObject>(of type: T.Type, where attribute: String, isEqualTo value: Any) -> T? {
        let predicate = NSPredicate(format: "%K == %@", argumentArray: [attribute, value])
        return self.findFirst(of: type, predicate: predicate)
    }

    func findFirst<T: NSManagedObject>(of type: T.Type, predicate: NSPredicate? = nil, offset: Int = 0) -> T? {
        let fetchRequest = self.fetchRequest(for: type)

        fetchRequest.predicate = predicate
        fetchRequest.fetchLimit = 1
        fetchRequest.fetchOffset = offset

        return self.findAll(with: fetchRequest).first
    }

    // MARK: - Count

    func count<T: NSManagedObject>(of type: T.Type) -> Int {
        return self.count(of: type, predicate: nil)
    }

    func count<T: NSManagedObject>(of type: T.Type, predicate: NSPredicate?) -> Int {
        let fetchRequest = self.fetchRequest(for: type, predicate: predicate)
        do {
            return try self.count(for: fetchRequest)
        } catch {
            return fatalIfDebug(error: error, orReturn: 0)
        }
    }

    private func fatalIfDebug<T>(error: Error, orReturn value: T) -> T {
        #if DEBUG
        fatalError(error.localizedDescription)
        #else
        return value
        #endif
    }
}
