//
//  NSManagedObjectContext+Observing.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 13.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

import CoreData
import ObjectiveC

private let kObserverContainerProperty = malloc(4)

extension NSManagedObjectContext {
    private var _observerContainer: ObserverContainer? {
        set {
            objc_setAssociatedObject(self, kObserverContainerProperty!, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
        get {
            return objc_getAssociatedObject(self, kObserverContainerProperty!) as? ObserverContainer
        }
    }

    @discardableResult
    public func observeNotification(forName name: Notification.Name, block: @escaping (Notification) -> Void) -> NSObjectProtocol {
        let notifyCenter = NotificationCenter.default

        let observer = notifyCenter.addObserver(forName: name, object: self, queue: nil, using: block)

        if _observerContainer == nil {
            _observerContainer = ObserverContainer(name: self.name ?? "unknown")
        }
        _observerContainer?.addObserver(observer)

        return observer
    }

    public func removeObserver(_ observer: NSObjectProtocol) {
        _observerContainer?.removeObserver(observer)
    }

    func clear() {
        self.reset()

        objc_setAssociatedObject(self, kObserverContainerProperty!, nil, .OBJC_ASSOCIATION_RETAIN)
    }
}

class ObserverContainer {
    private var _container: [NSObjectProtocol]

    private var name: String

    init(name: String) {
        self.name = name

        _container = [NSObjectProtocol]()
    }

    deinit {
        print("Removing \(_container.count) observers from '\(self.name)' context")

        let notifyCenter = NotificationCenter.default

        for object in _container {
            notifyCenter.removeObserver(object)
        }
    }

    func addObserver(_ observer: NSObjectProtocol) {
        _container.append(observer)
    }

    func removeObserver(_ observer: NSObjectProtocol) {
        NotificationCenter.default.removeObserver(observer)

        if let idx = _container.firstIndex(where: {return $0.isEqual(observer)}) {
            _container.remove(at: idx)
        }
    }
}
