//
//  NSPersistentStore+Url.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 13.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

extension NSPersistentStore {
    static func storeUrl(withName name: String, bundle: Bundle) -> URL {
        var url = self.defaultStoreDirectory(bundle: bundle)
        url.appendPathComponent(name, isDirectory: false)
        return url
    }

    static func defaultStoreDirectory(bundle: Bundle) -> URL {
        guard let execName = bundle.object(forInfoDictionaryKey: String(kCFBundleExecutableKey)) as? String else {
            fatalError("Cannot read \(String(kCFBundleExecutableKey)) key from info.plist file")
        }

        let fileManager = FileManager.default

        do {
            var url = try fileManager.url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            url.appendPathComponent(execName, isDirectory: true)
            url.appendPathComponent("CoreData", isDirectory: true)
            try fileManager.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)

            return url
        } catch {
            fatalError("Cannot get directory for persistent store")
        }
    }
}
