//
//  NSPersistentStoreCoordinator+Perform.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 05.01.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

extension NSPersistentStoreCoordinator {
    func performAndWaitOrThrow(_ block: () throws -> Swift.Void) throws -> Swift.Void {
        var error: Error?
        self.performAndWait {
            do {
                try block()
            } catch let localError {
                error = localError
            }
        }

        if let error = error {
            throw error
        }
    }
}
