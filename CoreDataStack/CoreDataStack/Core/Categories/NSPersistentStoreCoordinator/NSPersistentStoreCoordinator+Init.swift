//
//  NSManagedObjectStoreCoordinator+Init.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 13.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

extension NSPersistentStoreCoordinator {
    static func defaultCoordinator(model: NSManagedObjectModel, storeUrl: URL) -> NSPersistentStoreCoordinator {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: model)

        let options = [NSMigratePersistentStoresAutomaticallyOption: NSNumber(value: true),
                       NSInferMappingModelAutomaticallyOption: NSNumber(value: true)]
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeUrl, options: options)
        } catch let error as NSError {
            preconditionFailure("Cannot add persistent store at url: \(storeUrl.description) with error: \(error.localizedDescription)")
        }

        return coordinator
    }
}
