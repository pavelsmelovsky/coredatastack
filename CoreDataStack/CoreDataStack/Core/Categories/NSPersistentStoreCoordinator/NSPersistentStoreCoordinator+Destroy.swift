//
//  NSPersistentStoreCoordinator+Destroy.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 05.01.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

extension NSPersistentStoreCoordinator {
    func destroyAllStores() throws {
        try self.performAndWaitOrThrow {
            for store in self.persistentStores {
                let url = self.url(for: store)
                try self.destroyPersistentStore(at: url, ofType: store.type, options: store.options)
            }
        }
    }
}
