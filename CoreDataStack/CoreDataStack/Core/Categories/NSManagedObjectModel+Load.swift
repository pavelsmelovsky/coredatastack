//
//  NSManagedObjectModel+Load.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 13.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObjectModel {
    static func model(withName name: String, bundle: Bundle) -> NSManagedObjectModel {
        guard let modelUrl = bundle.url(forResource: name, withExtension: "momd") else {
            preconditionFailure("Cannot find model with name \(name)")
        }

        guard let model = NSManagedObjectModel(contentsOf: modelUrl) else {
            preconditionFailure("Cannot instantiate model with file \(modelUrl)")
        }

        return model
    }
}
