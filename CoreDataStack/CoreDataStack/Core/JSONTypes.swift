//
//  RSDefines.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 06.03.16.
//  Copyright © 2016 Smelovsky.Me. All rights reserved.
//

import Foundation

// JSON typealias

public typealias JSONArray                 = [Any]
public typealias JSONObject                = [String: Any]
public typealias JSONObjectArray           = [JSONObject]

public typealias JSONIntArray              = [Int]
public typealias JSONDoubleArray           = [Double]
public typealias JSONFloatArray            = [Float]
public typealias JSONBoolArray             = [Bool]
public typealias JSONStringArray           = [String]

public func doubleValue(_ value: Any?) -> Double {
    if let value = value as? Double {
        return value
    } else if let value = value as? String,
        let dValue = Double(value) {
        return dValue
    }
    return Double(0)
}
