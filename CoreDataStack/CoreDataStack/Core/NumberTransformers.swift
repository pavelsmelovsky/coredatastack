//
//  NumberTransformers.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 18.10.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

public protocol NumberStringConvertible {
    init?(_ text: String)
}

extension Int: NumberStringConvertible {}
extension Int8: NumberStringConvertible {}
extension Int16: NumberStringConvertible {}
extension Int32: NumberStringConvertible {}
extension Int64: NumberStringConvertible {}
extension UInt: NumberStringConvertible {}
extension UInt8: NumberStringConvertible {}
extension UInt16: NumberStringConvertible {}
extension UInt32: NumberStringConvertible {}
extension UInt64: NumberStringConvertible {}

extension Float: NumberStringConvertible {}
extension Double: NumberStringConvertible {}

extension Bool: NumberStringConvertible {}

extension CGFloat: NumberStringConvertible {
    public init?(_ text: String) {
        guard let double = Double(text) else { return nil }

        self.init(double)
    }
}

//extension NSDecimalNumber: NumberStringConvertible {
//    public convenience init?(_ text: String) {
//        self.init(string: text)
//    }
//}
