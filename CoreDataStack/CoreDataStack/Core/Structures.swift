//
//  Structures.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 09.12.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

public struct Sort {
    public var keyPath: String
    public var ascending: Bool

    public init(keyPath: String, ascending: Bool = true) {
        self.keyPath = keyPath
        self.ascending = ascending
    }
}

public struct Page {
    public var offset: Int
    public var limit: Int
}
