//
//  Assertions.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 30.12.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

func contextFailure(file: StaticString = #file, line: UInt = #line) -> Never {
    preconditionFailure("Context must be passed as parameter or instance CoreDataStack must be registered as `default`", file: file, line: line)
}
