//
//  CoreDataStack.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 08.10.16.
//  Copyright © 2016 Smelovsky.Me. All rights reserved.
//

import CoreData

public class CoreDataStack: NSObject {
    public static var `default`: CoreDataStack?

    public var useSingleImportContext: Bool = false {
        didSet {
            stack.useSingleSaveContext = useSingleImportContext
        }
    }

    public var automaticallyMergesChangesToViewContext: Bool = false {
        didSet {
            stack.automaticallyMergesChangesToViewContext = automaticallyMergesChangesToViewContext
        }
    }

    private var stack: CoreDataStackSubclass

    public init(name: String, url: URL? = nil, bundle: Bundle = .main) {
        self.stack = PersistentContainerStack(name: name, url: url, bundle: bundle)
    }

    public var viewContext: NSManagedObjectContext {
        return self.stack.viewContext
    }

    public func save(_ block: @escaping (NSManagedObjectContext) -> Void, completion: @escaping (Bool, Error?) -> Void) {
        self.stack.save(block, completion: completion)
    }

    public func saveAndWait(_ block: @escaping (NSManagedObjectContext) -> Void) throws {
        try self.stack.saveAndWait(block)
    }

    public func newBackgroundContext() -> NSManagedObjectContext {
        return self.stack.newBackgroundContext()
    }

    public func newBackgroundContext(named: String) -> NSManagedObjectContext {
        return self.stack.newBackgroundContext(name: named)
    }
}
