//
//  PersistentContainerStack.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 08.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import CoreData

class PersistentContainerStack: CoreDataStackSubclass {
    var automaticallyMergesChangesToViewContext: Bool = false {
        didSet {
            viewContext.automaticallyMergesChangesFromParent = automaticallyMergesChangesToViewContext
        }
    }

    var useSingleSaveContext: Bool = false {
        didSet {
            importContext = useSingleSaveContext ? newBackgroundContext(name: "singleImportContext") : nil
        }
    }

    private var persistentContainer: NSPersistentContainer!

    var viewContext: NSManagedObjectContext! {
        return self.persistentContainer.viewContext
    }

    private(set) var importContext: NSManagedObjectContext?

    convenience required init(name: String) {
        self.init(name: name, url: nil, bundle: Bundle.main)
    }

    convenience required init(name: String, bundle: Bundle) {
        self.init(name: name, url: nil, bundle: bundle)
    }

    convenience required init(name: String, url: URL?) {
        self.init(name: name, url: url, bundle: Bundle.main)
    }

    required init(name: String, url: URL?, bundle: Bundle) {
        let model = NSManagedObjectModel.model(withName: name, bundle: bundle)

        self.persistentContainer = NSPersistentContainer(name: name, managedObjectModel: model)

        let storeUrl = url ?? NSPersistentStore.storeUrl(withName: name, bundle: bundle)

        let sqlStoreDescription = NSPersistentStoreDescription.sql(at: storeUrl)

        sqlStoreDescription.setOption(nil, forKey: "")
        self.persistentContainer.persistentStoreDescriptions = [sqlStoreDescription]

        self.persistentContainer.loadPersistentStores { (storeDescription, error) in
            if error == nil {
                NSLog("\(storeDescription.description) has been loaded success")
            }
        }

        viewContext.name = "viewContext"
        viewContext.obtainPermanentIds()
    }

    func destroyStores() throws {
        let coordinator = self.persistentContainer.persistentStoreCoordinator
        try coordinator.destroyAllStores()
    }

    func newBackgroundContext() -> NSManagedObjectContext {
        return self.newBackgroundContext(name: "background")
    }

    func newBackgroundContext(name: String) -> NSManagedObjectContext {
        print("Create background context named \(name)")

        let ctx = self.persistentContainer.newBackgroundContext()
        ctx.name = name
        ctx.obtainPermanentIds()
        return ctx
    }
}
