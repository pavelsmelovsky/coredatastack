//
//  CoreDataStackDependency.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 30.12.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

public protocol CoreDataStackDependency: class {
    var stack: CoreDataStack! { get set }
}
