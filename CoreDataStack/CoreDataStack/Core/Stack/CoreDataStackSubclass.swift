//
//  CoreDataStackSubclass.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 08.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation

import CoreData

protocol CoreDataStackSubclass {
    var viewContext: NSManagedObjectContext! { get }
    var importContext: NSManagedObjectContext? { get }

    var useSingleSaveContext: Bool { get set }
    var automaticallyMergesChangesToViewContext: Bool { get set }

    init(name: String)
    init(name: String, bundle: Bundle)

    init(name: String, url: URL?)
    init(name: String, url: URL?, bundle: Bundle)

    func destroyStores() throws

    func save(_ block: @escaping (_ context: NSManagedObjectContext) -> Void, completion: @escaping (_ success: Bool, _ error: Error?) -> Void)

    func saveAndWait(_ block: @escaping (_ context: NSManagedObjectContext) -> Void) throws

    func newBackgroundContext() -> NSManagedObjectContext
    func newBackgroundContext(name: String) -> NSManagedObjectContext
}

extension CoreDataStackSubclass {
    private func _importContext() -> NSManagedObjectContext {
        return importContext ?? newBackgroundContext(name: "importContext")
    }

    func save(_ block: @escaping (NSManagedObjectContext) -> Void, completion: @escaping (Bool, Error?) -> Void) {
        let context = _importContext()

        context.perform {
            block(context)

            context.saveToPersistentStore(completion: { (success, error) in
                DispatchQueue.main.async {
                    completion(success, error)
                }
            })
        }
    }

    func saveAndWait(_ block: @escaping (NSManagedObjectContext) -> Void) throws {
        let context = _importContext()

        try context.performAndWaitOrThrow {
            block(context)

            try context.saveToPersistentStoreAndWait()
        }
    }
}
