//
//  CoreDataStack.h
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 17.05.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoreDataStack.
FOUNDATION_EXPORT double CoreDataStackVersionNumber;

//! Project version string for CoreDataStack.
FOUNDATION_EXPORT const unsigned char CoreDataStackVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreDataStack/PublicHeader.h>


