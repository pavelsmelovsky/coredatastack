//
//  NSPropertyDescription+Attributes.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 12.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

extension NSPropertyDescription {
    var mappedKeyName: String {
        return (self.userInfo?[Constants.mappedKeyName] as? String) ?? self.name
    }

    var dateFormat: String {
        return (self.userInfo?[Constants.dateFormat] as? String) ?? Constants.defaultDateFormat
    }
}
