//
//  Functions.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 29.06.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

typealias Function = @convention(c) (AnyObject, Selector, Any) -> Bool
typealias ImportBlock = (AnyObject, Any) -> Bool

func instanceMethod(for aClass: AnyClass, selector: Selector) -> ImportBlock? {
    guard let method = class_getInstanceMethod(aClass, selector) else { return nil }

    let implementation = method_getImplementation(method)
    let function = unsafeBitCast(implementation, to: Function.self)
    return { object, value in function(object, selector, value) }
}
