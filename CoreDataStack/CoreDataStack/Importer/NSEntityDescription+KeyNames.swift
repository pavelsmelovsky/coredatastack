//
//  NSEntityDescription+KeyNames.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 12.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

extension NSEntityDescription {
    public var relatedByAttribute: NSAttributeDescription? {
        if let name = self.userInfo?[Constants.relatedByAttribute] as? String {
            return self.attributesByName[name]
        }
        return self.superentity?.relatedByAttribute
    }

    public var sortByAttribute: NSAttributeDescription? {
        if let name = self.userInfo?[Constants.sortByAttribute] as? String {
            return self.attributesByName[name]
        }
        return nil
    }
}
