//
//  CoreDataSyncer.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 25/12/2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

open class CoreDataSyncer<ObjClass: NSManagedObject, SortPropertyType: Comparable>: CoreDataImporterBase {
    private struct SyncRange<T: Comparable> {
        public let begin: T
        public let end: T

        init(begin: T, end: T) {
            self.begin = begin
            self.end = end
        }

        var isAscending: Bool {
            return self.begin < self.end
        }
    }

    var objects: [ObjClass]?

    private var json: [[String: Any]]
    private var type: ObjClass.Type
    private var sortPropertyType: SortPropertyType.Type

    private var sortProperty: String!

    public init(json: [[String: Any]], type: ObjClass.Type, sortPropertyType: SortPropertyType.Type, dateFormatter: DateFormatter?, context: NSManagedObjectContext) {
        self.json = json
        self.type = type
        self.sortPropertyType = sortPropertyType

        super.init(context: context, dateFormatter: dateFormatter)

        self.initialSetup()
    }

    internal init(json: [[String: Any]], type: ObjClass.Type, sortPropertyType: SortPropertyType.Type, parent: CoreDataImporterBase) {
        self.json = json
        self.type = type
        self.sortPropertyType = sortPropertyType

        super.init(parent: parent)

        self.initialSetup()
    }

    private func initialSetup() {
        let entity = type.entity()

        guard let sortProperty = entity.userInfo?[Constants.sortByAttribute] as? String else {
            fatalError("sortByAttribute must be specified in Entity")
        }

        self.sortProperty = sortProperty

        // add blocks only if this has not been added earlier
        guard self.customPropertyBlocks.index(forKey: entity.name!) == nil else { return }

        debugPrint("Generate property blocks for entity: \(entity.name!)")

        var blocks: [NSPropertyDescription: ImportBlock] = [:]
        for property in entity.properties {
            let selector = "import\(property.name.firstLetterUppercased):"
            if let block = instanceMethod(for: type, selector: Selector(selector)) {
                blocks[property] = block
            }
        }

        self.add(blocks: blocks, for: entity)
    }

    private var startValue: Any?
    private var comparableAscending: Bool = false

    public func sync(from: SortPropertyType, userPredicate: NSPredicate? = nil) {
        let entity = self.type.entity()

        guard let relatedAttribute = entity.relatedByAttribute else {
            fatalError("Entity \(entity.name!) does not contains related attribute")
        }

        guard let sortAttribute = entity.sortByAttribute else {
            fatalError("Entity \(entity.name!) must include 'sortByAttribute' key with valid attribute name")
        }

        let mappedKeyName = relatedAttribute.mappedKeyName

        var predicates = [NSPredicate]()

        let range = self.range(sortAttribute)
        // from must be exclusive
        let rangePredicate: NSPredicate
        if range.isAscending {
            rangePredicate = NSPredicate(format: "%K > %@ && %K <= %@", argumentArray: [sortAttribute.name, from, sortAttribute.name, range.end])
        } else {
            rangePredicate = NSPredicate(format: "%K >= %@ && %K < %@", argumentArray: [sortAttribute.name, range.end, sortAttribute.name, from])
        }
        predicates.append(rangePredicate)

        if let userPredicate = userPredicate {
            predicates.append(userPredicate)
        }

        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        let existingItems = self.context.findAll(of: self.type, with: predicate)

        var jsonMap = [AnyHashable: [String: Any]]()
        self.json.forEach { (item) in
            guard let key = item[mappedKeyName] as? AnyHashable else { return }

            jsonMap[key] = item
        }

        var existingMap = [AnyHashable: ObjClass]()
        existingItems.forEach { (item) in
            guard let key = item.value(forKey: relatedAttribute.name) as? AnyHashable else { return }

            existingMap[key] = item
        }

        existingMap.forEach { (key, item) in
            if let json = jsonMap.removeValue(forKey: key) {
                let importer = NSManagedObjectImporter(object: item, json: json, importer: self)
                importer.update()
            } else {
                self.context.delete(item)
            }
        }

        jsonMap.forEach { (entry) in
            self.createObject(entity: entity, jsonItem: entry.value)
        }
    }

    private func createObject(entity: NSEntityDescription, jsonItem: [String: Any]) {
        let object = NSManagedObject(entity: entity, insertInto: self.context)

        let objectImporter = NSManagedObjectImporter(object: object, json: jsonItem, importer: self)
        objectImporter.update()
    }

    private func range(_ attribute: NSAttributeDescription) -> SyncRange<SortPropertyType> {
        let sortKey = attribute.mappedKeyName

        guard let first = self.json.first?[sortKey],
            let last = self.json.last?[sortKey] else {
                fatalError("Json must contains \(sortKey) property")
        }

        if self.sortPropertyType == Date.self {
            return self.datesRange(firstValue: first, lastValue: last, dateFormat: attribute.dateFormat)
        } else if let firstValue = first as? SortPropertyType, let lastValue = last as? SortPropertyType {
            return SyncRange(begin: firstValue, end: lastValue)
        }

        fatalError("Incorrect Sort property type")
    }

    private func datesRange(firstValue: Any, lastValue: Any, dateFormat: String) -> SyncRange<SortPropertyType> {
        if let firstStr = firstValue as? String, let lastStr = lastValue as? String {
            let formatter = self.dateFormatter(dateFormat: dateFormat)
            guard let firstDate = formatter.date(from: firstStr) as? SortPropertyType,
                let lastDate = formatter.date(from: lastStr) as? SortPropertyType else {
                    fatalError("Incorrect date format")
            }
            return SyncRange(begin: firstDate, end: lastDate)
        } else if let firstNumber = firstValue as? Int, let lastNumber = lastValue as? Int {
            guard let firstDate = Date(timeIntervalSince1970: TimeInterval(firstNumber)) as? SortPropertyType,
                let lastDate = Date(timeIntervalSince1970: TimeInterval(lastNumber)) as? SortPropertyType else {
                    fatalError("Incorrect types")
            }
            return SyncRange(begin: firstDate, end: lastDate)
        } else {
            fatalError("Incorrect date-time argument in json")
        }
    }
}
