//
//  Constants.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 28.06.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

struct Constants {
    static let relatedByAttribute = "relatedByAttribute"
    static let mappedKeyName = "mappedKeyName"
    static let dateFormat = "dateFormat"

    static let sortByAttribute = "sortByAttribute"

    static let defaultDateFormat = "yyyy-MM-dd'T'HH:mm:ssSSSZZZ"
}
