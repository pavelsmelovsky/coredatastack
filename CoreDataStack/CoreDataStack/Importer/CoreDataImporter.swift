//
//  CoreDataImporter.swift
//  CoreDataStack+Importer
//
//  Created by Pavel Smelovsky on 22.05.2018.
//  Copyright © 2018 Smelovsky.me. All rights reserved.
//

import Foundation
import CoreData

struct CustomPropertyBlocks {
    private var blocks: [NSPropertyDescription: ImportBlock]

    init(blocks: [NSPropertyDescription: ImportBlock]) {
        self.blocks = blocks
    }

    func block(for property: NSPropertyDescription) -> ImportBlock? {
        return blocks[property]
    }
}

open class CoreDataImporterBase {
    private(set) var parent: CoreDataImporterBase?

    public var context: NSManagedObjectContext {
        if let parent = parent {
            return parent.context
        }
        return _context
    }

    public var defaultDateFormatter: DateFormatter? {
        if let parent = self.parent {
            return parent.defaultDateFormatter
        }
        return _defaultDateFormatter
    }

    private var _context: NSManagedObjectContext!
    private var _defaultDateFormatter: DateFormatter?
    private var _dateFormatters: [String: DateFormatter] = [:]

    /// Key: Entity name, value: struct contains all properties with custom import blocks
    private var _customPropertyBlocks: [String: CustomPropertyBlocks]!
    internal var customPropertyBlocks: [String: CustomPropertyBlocks] {
        if let parent = parent {
            return parent.customPropertyBlocks
        } else {
            return _customPropertyBlocks
        }
    }

    init(parent: CoreDataImporterBase) {
        self.parent = parent
    }

    init(context: NSManagedObjectContext, dateFormatter: DateFormatter?) {
        _context = context
        _defaultDateFormatter = dateFormatter

        _customPropertyBlocks = [:]
    }

    func add(blocks: [NSPropertyDescription: ImportBlock], for entity: NSEntityDescription) {
        if let parent = self.parent {
            parent.add(blocks: blocks, for: entity)
        } else {
            _customPropertyBlocks[entity.name!] = CustomPropertyBlocks(blocks: blocks)
        }
    }

    func dateFormatter(dateFormat: String) -> DateFormatter {
        if let parent = self.parent {
            return parent.dateFormatter(dateFormat: dateFormat)
        }

        if let dateFormatter = _dateFormatters[dateFormat] {
            return dateFormatter
        }

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        _dateFormatters[dateFormat] = dateFormatter
        return dateFormatter
    }

    internal func importBlock(for property: NSPropertyDescription) -> ImportBlock? {
        return self.customPropertyBlocks[property.entity.name!]?.block(for: property)
    }
}

open class CoreDataImporter<ObjClass: NSManagedObject>: CoreDataImporterBase {
    var objects: [ObjClass]?

    private var json: [[String: Any]]
    private var type: ObjClass.Type

    public init(json: [[String: Any]], type: ObjClass.Type, dateFormatter: DateFormatter?, context: NSManagedObjectContext) {
        self.json = json
        self.type = type

        super.init(context: context, dateFormatter: dateFormatter)

        self.generateCustomImportBlocks()
    }

    internal init(json: [[String: Any]], type: ObjClass.Type, parent: CoreDataImporterBase) {
        self.json = json
        self.type = type

        super.init(parent: parent)

        self.generateCustomImportBlocks()
    }

    private func generateCustomImportBlocks() {
        let entity = type.entity()

        // add blocks only if this has not been added earlier
        guard self.customPropertyBlocks.index(forKey: entity.name!) == nil else { return }

        debugPrint("Generate property blocks for entity: \(entity.name!)")

        var blocks: [NSPropertyDescription: ImportBlock] = [:]
        for property in entity.properties {
            let selector = "import\(property.name.firstLetterUppercased):"
            if let block = instanceMethod(for: type, selector: Selector(selector)) {
                blocks[property] = block
            }
        }

        self.add(blocks: blocks, for: entity)
    }

    @discardableResult
    public func parse(predicate: NSPredicate? = nil) -> [ObjClass] {
        var result: [ObjClass] = []

        let entity = self.type.entity()

        guard let relatedAttribute = entity.relatedByAttribute else {
            fatalError("Entity \(entity.name!) does not contains related attribute")
        }

        let mappedKeyName = relatedAttribute.mappedKeyName

        for item in json {
            guard let relatedValue = item[mappedKeyName] else { continue }

            let predicate = self.buildPredicate(for: relatedAttribute, isEqualTo: relatedValue, additionalPredicate: predicate)
            var object = self.type.findFirst(predicate: predicate, in: self.context)
            if object == nil {
                object = self.type.init(entity: entity, insertInto: self.context)
            }

            if let object = object {
                let objectImporter = NSManagedObjectImporter(object: object, json: item, importer: self)
                objectImporter.update()
                result.append(object)
            }
        }

        return result
    }

    public func delete(with customPredicate: NSPredicate? = nil) {
        let entity = self.type.entity()
        guard let relatedAttribute = entity.relatedByAttribute else {
            fatalError()
        }

        let mappedKeyName = relatedAttribute.mappedKeyName

        let identifiers = self.json.map { item -> Any in
            guard let name = item[mappedKeyName] else { fatalError("Json for \(type.entityName) does not contains property \(mappedKeyName)") }
            return name
        }

        let notInPredicate = NSPredicate(format: "NOT(%K IN %@)", relatedAttribute.name, identifiers)

        var predicates: [NSPredicate] = []
        predicates.append(notInPredicate)

        if let customPredicate = customPredicate {
            predicates.append(customPredicate)
        }

        let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)

        let fetchRequest = NSFetchRequest<ObjClass>(entityName: self.type.entityName)
        fetchRequest.predicate = predicate
        do {
            let objects = try self.context.fetch(fetchRequest)
            objects.forEach({self.context.delete($0)})
        } catch {
            print("Cannot fetch objects with predicate: \(predicate)")
        }
    }

    private func buildPredicate(for relatedAttribute: NSPropertyDescription, isEqualTo value: Any, additionalPredicate: NSPredicate?) -> NSPredicate {
        let attributePredicate = NSPredicate(format: "%K = %@", argumentArray: [relatedAttribute.name, value])
        if let additionalPredicate = additionalPredicate {
            return NSCompoundPredicate(andPredicateWithSubpredicates: [additionalPredicate, attributePredicate])
        }
        return attributePredicate
    }
}
