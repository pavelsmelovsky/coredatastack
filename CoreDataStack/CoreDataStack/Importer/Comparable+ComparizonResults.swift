//
//  Comparable+ComparizonResults.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 25/12/2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

extension Comparable {
    func compareWith(_ value: Self) -> ComparisonResult {
        if self < value {
            return ComparisonResult.orderedAscending
        } else if self > value {
            return ComparisonResult.orderedDescending
        } else {
            return ComparisonResult.orderedSame
        }
    }
}
