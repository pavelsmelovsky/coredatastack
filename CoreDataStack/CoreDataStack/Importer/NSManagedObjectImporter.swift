//
//  NSManagedObjectImporter.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 15.12.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

class NSManagedObjectImporter<ObjectType: NSManagedObject> {
    private var object: ObjectType
    private var json: [String: Any]
    private unowned var importer: CoreDataImporterBase

    init(object: ObjectType, json: [String: Any], importer: CoreDataImporterBase) {
        self.object = object
        self.json = json
        self.importer = importer
    }

    func update() {
        let entity = object.entity

        for property in entity.properties {
            if property.isTransient {
                continue
            }

            // skip automatic property import, if custom import available
            if self.customImport(property) {
                continue
            }

            guard let value = json[property.mappedKeyName] else {
                continue
            }

            if let attribute = property as? NSAttributeDescription {
                self.update(value: value, forAttribute: attribute)
            } else if let relationship = property as? NSRelationshipDescription {
                self.update(value: value, forRelationship: relationship)
            }
        }
    }

    private func customImport(_ property: NSPropertyDescription) -> Bool {
        guard let block = importer.importBlock(for: property) else { return false }

        return block(object, json)
    }

    private func update(value: Any, forAttribute attribute: NSAttributeDescription) {
        var result: Any?

        if value is NSNull {
            result = attribute.defaultValue
        } else {
            result = parse(value: value, forAttributeType: attribute.attributeType, attributeInfo: attribute.userInfo)
        }

        object.setValue(result, forKey: attribute.name)
    }

    // swiftlint:disable cyclomatic_complexity
    // swiftlint:disable function_body_length
    private func parse(value: Any, forAttributeType attributeType: NSAttributeType, attributeInfo: [AnyHashable: Any]?) -> Any? {
        switch attributeType {
        case .doubleAttributeType:
            return castValue(value: value) as Double?
        case .floatAttributeType:
            return castValue(value: value) as Float?
        case .integer16AttributeType:
            return castValue(value: value) as Int16?
        case .integer32AttributeType:
            return castValue(value: value) as Int32?
        case .integer64AttributeType:
            return castValue(value: value) as Int64?
        case .decimalAttributeType:
            if let value = value as? NSDecimalNumber {
                return value
            } else if let value = value as? String {
                return NSDecimalNumber(string: value)
            }
        case .booleanAttributeType:
            return castValue(value: value) as Bool?
        case .dateAttributeType:
            return self.parseDate(value: value, attributeInfo: attributeInfo)
        case .stringAttributeType:
            if let value = value as? String {
                return value
            } else if let value = value as? CustomStringConvertible {
                return value.description
            }
        case .undefinedAttributeType:
            return value
        case .binaryDataAttributeType:
            if let value = value as? String {
                return value.data(using: .utf8)
            } else {
                return nil
            }
        case .UUIDAttributeType:
            if let value = value as? String {
                return UUID(uuidString: value)
            } else {
                return nil
            }
        case .URIAttributeType:
            if let value = value as? String {
                return URL(string: value)
            } else {
                return nil
            }
        case .transformableAttributeType:
            return nil
        case .objectIDAttributeType:
            fatalError("Cannot parse objectIDAttributeType")
        @unknown default:
            #if DEBUG
            fatalError("Unknown type")
            #else
            break
            #endif
        }

        return nil
    }
    // swiftlint:enable cyclomatic_complexity
    // swiftlint:enable function_body_length

    private func castValue<T>(value: Any?) -> T? where T: NumberStringConvertible {
        if let value = value as? T {
            return value
        } else if let value = value as? String {
            return T(value)
        }
        return nil
    }

    private func parseDate(value: Any, attributeInfo: [AnyHashable: Any]?) -> Date? {
        if let value = value as? TimeInterval {
            return Date(timeIntervalSince1970: value)
        } else if let value = value as? String {
            let formatter: DateFormatter?

            if let dateFormat = attributeInfo?[Constants.dateFormat] as? String {
                formatter = importer.dateFormatter(dateFormat: dateFormat)
            } else {
                formatter = importer.defaultDateFormatter
            }

            return formatter?.date(from: value)
        }
        return nil
    }

    private func update(value: Any, forRelationship relationship: NSRelationshipDescription) {
        // relationship related to current object from destination entity
        let inverseRelationship = relationship.inverseRelationship

        // destination entity that related with current object
        guard let dstEntity = relationship.destinationEntity else {
            fatalError("Destination entity cannot be nil")
        }

        guard let dstKlass = NSClassFromString(dstEntity.managedObjectClassName) as? NSManagedObject.Type else {
            fatalError("Class of destination entity must conforms to NSManagedObject protocol")
        }

        if value is NSNull {
            self.clear(relationship: relationship)
        } else if relationship.isToMany,
            let value = value as? JSONObjectArray {

            let importer = CoreDataImporter(json: value, type: dstKlass, parent: self.importer)
            let imported = importer.parse()

            let mutableSet = object.mutableSetValue(forKey: relationship.name)
            mutableSet.addObjects(from: imported)

            if let inverseRelationship = inverseRelationship,
                !inverseRelationship.isToMany {
                let predicate = NSPredicate(format: "%K == %@", inverseRelationship.name, object)
                importer.delete(with: predicate)
            }
        } else if !relationship.isToMany, let value = value as? JSONObject {
            let oldRelatedObject = object.value(forKey: relationship.name) as? NSManagedObject

            let importer = CoreDataImporter(json: [value], type: dstKlass, parent: self.importer)
            let imported = importer.parse()
            if let relatedObject = imported.first {
                object.setValue(relatedObject, forKey: relationship.name)

                // FIXME: Should we delete related object? It's seems that this object can be related with other objects
                let isOptional = inverseRelationship?.isOptional ?? true
                if let oldRelatedObject = oldRelatedObject,
                    oldRelatedObject != relatedObject,
                    !isOptional {// remove related object only if reverse relationship is not optional
                    // to prevent null pointer exception
                    self.importer.context.delete(oldRelatedObject)
                }
            }
        }
    }

    private func clear(relationship: NSRelationshipDescription) {
        if relationship.isToMany {
            self.clear2many(relationship: relationship)
        } else {
            self.clear2one(relationship: relationship)
        }
    }

    private func clear2one(relationship: NSRelationshipDescription) {
        if relationship.deleteRule == .cascadeDeleteRule {
            if let relatedObject = object.value(forKey: relationship.name) as? NSManagedObject {
                self.importer.context.delete(relatedObject)
            }
        } else if relationship.deleteRule == .nullifyDeleteRule {
            object.setNilValueForKey(relationship.name)
        }
    }

    private func clear2many(relationship: NSRelationshipDescription) {
        let relatedObjects = object.mutableSetValue(forKey: relationship.name)
        if relationship.deleteRule == .cascadeDeleteRule {
            relatedObjects.forEach { object in
                if let object = object as? NSManagedObject {
                    self.importer.context.delete(object)
                }
            }
        }

        relatedObjects.removeAllObjects()
    }
}
