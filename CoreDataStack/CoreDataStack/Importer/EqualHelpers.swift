//
//  EqualHelpers.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 17.12.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import Foundation
import CoreData

/// method to equal CoreData and JSON values
/// now supported only to types - Int and String
internal func isValue(_ value1: Any, equalTo value2: Any, of type: NSAttributeType) -> Bool {
    switch type {
    case .integer16AttributeType, .integer32AttributeType, .integer64AttributeType:
        return value(value1 as? Int, isEqualTo: value2 as? Int)
    case .stringAttributeType:
        return value(value1 as? String, isEqualTo: value2 as? String)
    default: return false
    }
}

internal func value<T: Equatable>(_ value1: T?, isEqualTo value2: T?) -> Bool {
    return value1 == value2
}

internal func compare(_ value1: Any?, to value2: Any?, of type: NSAttributeType) -> ComparisonResult {
    switch type {
    case .integer16AttributeType, .integer32AttributeType, .integer64AttributeType:
        guard let int1 = value1 as? Int,
            let int2 = value2 as? Int else {
            fatalError("Invalid value types")
        }
        return compare(int1, to: int2)
    case .stringAttributeType:
        guard let string1 = value1 as? String,
            let string2 = value2 as? String else {
                fatalError("Invalid value types")
        }
        return compare(string1, to: string2)
    default:
        fatalError("Unsupported attribute type")
    }
}

internal func compare<T: Comparable>(_ value1: T, to value2: T) -> ComparisonResult {
    if value1 < value2 {
        return .orderedAscending
    } else if value1 > value2 {
        return .orderedDescending
    } else {
        return .orderedSame
    }
}
