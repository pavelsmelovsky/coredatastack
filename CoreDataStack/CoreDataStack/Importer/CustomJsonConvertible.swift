//
//  CustomJsonConvertible.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 29.03.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import Foundation

public protocol CustomJsonConvertible {
    var json: [String: Any] { get }
}

extension Dictionary: CustomJsonConvertible where Key == String, Value == Any {
    public var json: [Key: Value] {
        return self
    }
}
