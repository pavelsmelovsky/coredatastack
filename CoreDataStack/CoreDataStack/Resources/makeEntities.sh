#!/bin/sh

mogenerator \
--template-path=. \
--model Roistat.xcdatamodeld \
--output-dir ./Entities \
--swift \
--template-var scalarsWhenNonOptional=true \
--machine-dir ./Entities/BaseEntities \
--human-dir ./Entities
