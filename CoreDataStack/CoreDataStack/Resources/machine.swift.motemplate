// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to <$sanitizedManagedObjectClassName$>.swift instead.

import Foundation
import CoreData
import PSCoreDataStack

<$if hasCustomBaseCaseImport$>import <$baseClassImport$><$endif$>


<$if hasCustomSuperentity $>
open class _<$sanitizedManagedObjectClassName$>: <$customSuperentity$>, NSManagedObjectSubclass {
<$else$>
open class _<$sanitizedManagedObjectClassName$>: NSManagedObject, NSManagedObjectSubclass {
<$endif$>
    // MARK: - Class methods

    <$if (hasCustomSuperclass || (hasCustomSuperentity && TemplateVar.overrideBaseClass))$>override <$endif$>open class var entityName: String {
        return "<$name$>"
    }

    <$if (hasCustomSuperclass || (hasCustomSuperentity && TemplateVar.overrideBaseClass))$>override <$endif$>open class func entity(managedObjectContext: NSManagedObjectContext) -> NSEntityDescription? {
        return NSEntityDescription.entity(forEntityName: self.entityName, in: managedObjectContext)
    }

    // MARK: - Attributes and relationships definitions

    <$if noninheritedAttributes.@count > 0$>
    struct Attributes {<$foreach Attribute noninheritedAttributes do$>
        static let <$Attribute.name$> = "<$Attribute.name$>"<$endforeach do$>
    }
    <$endif$>

    <$if noninheritedRelationships.@count > 0$>
    struct Relationships {<$foreach Relationship noninheritedRelationships do$>
        static let <$Relationship.name$> = "<$Relationship.name$>"<$endforeach do$>
    }
    <$endif$>

    <$if noninheritedFetchedProperties.@count > 0$>
    struct FetchedProperties {<$foreach FetchedProperty noninheritedFetchedProperties do$>
        static let <$FetchedProperty.name$> = "<$FetchedProperty.name$>"<$endforeach do$>
    }
    <$endif$>

    <$if hasUserInfoKeys && userInfoKeyValues.@count > 0$>
    struct UserInfo {<$foreach UserInfo userInfoKeyValues do$>
        static let <$UserInfo.key$> = "<$UserInfo.key$>"
        static let <$UserInfo.key$>Value = "<$UserInfo.value$>"<$endforeach do$>
    }
    <$endif$>

    // MARK: - Life cycle methods

    public override init(entity: NSEntityDescription, insertInto context: NSManagedObjectContext?) {
        super.init(entity: entity, insertInto: context)
    }

    // MARK: - Properties
<$foreach Attribute noninheritedAttributes do$>
<$if Attribute.hasDefinedAttributeType$>
<$if Attribute.hasScalarAttributeType$>
<$if Attribute.isReadonly$>
    open var <$Attribute.name$>: <$if Attribute.usesScalarAttributeType$><$Attribute.scalarAttributeType$><$if Attribute.isOptional$> // Optional scalars not supported<$endif$><$else$>NSNumber<$if Attribute.isOptional$>?<$else$>!<$endif$><$endif$>
    {
        self.willAccessValue(forKey: <$sanitizedManagedObjectClassName$>Attributes.<$Attribute.name$>.rawValue)
        let <$Attribute.name$> = self.primitiveValue(forKey: <$sanitizedManagedObjectClassName$>Attributes.<$Attribute.name$>.rawValue) as? <$if Attribute.usesScalarAttributeType$><$Attribute.scalarAttributeType$><$else$>NSNumber<$endif$>
        self.didAccessValue(forKey: <$sanitizedManagedObjectClassName$>Attributes.<$Attribute.name$>.rawValue)
        return <$Attribute.name$>
    }
<$else$>
    @NSManaged open
    var <$Attribute.name$>: <$if Attribute.usesScalarAttributeType$><$Attribute.scalarAttributeType$><$if Attribute.isOptional$> // Optional scalars not supported<$endif$><$else$>NSNumber<$if Attribute.isOptional$>?<$else$>!<$endif$><$endif$>
<$endif$>
<$else$>
<$if Attribute.isReadonly$>
    open var <$Attribute.name$>: <$Attribute.objectAttributeType$><$if Attribute.isOptional$>?<$else$>!<$endif$>
    {
        self.willAccessValue(forKey: <$sanitizedManagedObjectClassName$>Attributes.<$Attribute.name$>.rawValue)
        let <$Attribute.name$> = self.primitiveValue(forKey: <$sanitizedManagedObjectClassName$>Attributes.<$Attribute.name$>.rawValue) as? <$Attribute.objectAttributeType$>
        self.didAccessValue(forKey: <$sanitizedManagedObjectClassName$>Attributes.<$Attribute.name$>.rawValue)
        return <$Attribute.name$>
    }
<$else$>
    @NSManaged open
    var <$Attribute.name$>: <$Attribute.objectAttributeType$><$if Attribute.isOptional$>?<$else$>!<$endif$>
<$endif$>
<$endif$>
<$endif$>
<$endforeach do$>

    // MARK: - Relationships
<$foreach Relationship noninheritedRelationships do$>
<$if Relationship.isToMany$>
    @NSManaged open
    var <$Relationship.name$>: <$Relationship.immutableCollectionClassName$>

    open func <$Relationship.name$>Set() -> <$Relationship.mutableCollectionClassName$> {
        return self.<$Relationship.name$>.mutableCopy() as! <$Relationship.mutableCollectionClassName$>
    }

<$else$>
    @NSManaged open
    var <$Relationship.name$>: <$Relationship.destinationEntity.sanitizedManagedObjectClassName$><$if Relationship.isOptional$>?<$endif$>
<$endif$>
<$endforeach do$>

<$foreach FetchRequest prettyFetchRequests do$>
<$if FetchRequest.singleResult$>
    class func fetch<$FetchRequest.name.initialCapitalString$>(managedObjectContext: NSManagedObjectContext<$foreach Binding FetchRequest.bindings do2$>, <$Binding.name$>: <$Binding.type$><$endforeach do2$>) -> Any? {
        return self.fetch<$FetchRequest.name.initialCapitalString$>(managedObjectContext: managedObjectContext<$foreach Binding FetchRequest.bindings do2$>, <$Binding.name$>: <$Binding.name$><$endforeach do2$>, error: nil)
    }

    class func fetch<$FetchRequest.name.initialCapitalString$>(managedObjectContext: NSManagedObjectContext<$foreach Binding FetchRequest.bindings do2$>, <$Binding.name$>: <$Binding.type$><$endforeach do2$>, error outError: NSErrorPointer) -> Any? {
        guard let psc = managedObjectContext.persistentStoreCoordinator else { return nil }
        let model = psc.managedObjectModel
        let substitutionVariables : [String : Any] = [<$if FetchRequest.hasBindings$><$foreach Binding FetchRequest.bindings do2$>
                    "<$Binding.name$>": <$Binding.name$>,
        <$endforeach do2$><$else$>:<$endif$>]

        guard let fetchRequest = model.fetchRequestFromTemplate(withName: "<$FetchRequest.name$>", substitutionVariables: substitutionVariables) else {
          assert(false, "Can't find fetch request named \"<$FetchRequest.name$>\".")
          return nil
        }

        var result: Any? = nil
        do {
            let results = try managedObjectContext.fetch(fetchRequest)
            switch results.count {
            case 0:
                // Nothing found matching the fetch request. That's cool, though: we'll just return nil.
                break
            case 1:
                result = results.first
            default:
                print("WARN fetch request <$FetchRequest.name$>: 0 or 1 objects expected, \(results.count) found (substitutionVariables: \(substitutionVariables), results: \(results))")
            }

        } catch {
            print("Error executing fetch request: \(error)")
        }
        return result
    }
<$else$>
    class func fetch<$FetchRequest.name.initialCapitalString$>(managedObjectContext: NSManagedObjectContext<$foreach Binding FetchRequest.bindings do2$>, <$Binding.name$>: <$Binding.type$><$endforeach do2$>) -> [Any]? {
        return self.fetch<$FetchRequest.name.initialCapitalString$>(managedObjectContext: managedObjectContext<$foreach Binding FetchRequest.bindings do2$>, <$Binding.name$>: <$Binding.name$><$endforeach do2$>, error: nil)
    }

    class func fetch<$FetchRequest.name.initialCapitalString$>(managedObjectContext: NSManagedObjectContext<$foreach Binding FetchRequest.bindings do2$>, <$Binding.name$>: <$Binding.type$><$endforeach do2$>, error outError: NSErrorPointer) -> [Any]? {
        guard let psc = managedObjectContext.persistentStoreCoordinator else { return nil }
        let model = psc.managedObjectModel
        let substitutionVariables : [String : Any] = [<$if FetchRequest.hasBindings$><$foreach Binding FetchRequest.bindings do2$>
            "<$Binding.name$>": <$Binding.name$>,
<$endforeach do2$><$else$>:<$endif$>]

        guard let fetchRequest = model.fetchRequestFromTemplate(withName: "<$FetchRequest.name$>", substitutionVariables: substitutionVariables) else {
            assert(false, "Can't find fetch request named \"<$FetchRequest.name$>\".")
        return nil
    }
        var results = Array<Any>()
        do {
             results = try managedObjectContext.fetch(fetchRequest)
        } catch {
          print("Error executing fetch request: \(error)")
        }

        return results
    }
<$endif$>
<$endforeach do$>

<$foreach FetchedProperty noninheritedFetchedProperties do$>
    @NSManaged open
    let <$FetchedProperty.name$>: [<$FetchedProperty.entity.sanitizedManagedObjectClassName$>]
<$endforeach do$>
}

<$foreach Relationship noninheritedRelationships do$><$if Relationship.isToMany$>
extension _<$sanitizedManagedObjectClassName$> 
{
    @objc(add<$Relationship.name.initialCapitalString$>Object:)
    @NSManaged public func addTo<$Relationship.name.initialCapitalString$>(_ value: <$Relationship.destinationEntity.sanitizedManagedObjectClassName$>)

    @objc(remove<$Relationship.name.initialCapitalString$>Object:)
    @NSManaged public func removeFrom<$Relationship.name.initialCapitalString$>(_ value: <$Relationship.destinationEntity.sanitizedManagedObjectClassName$>)

    @objc(add<$Relationship.name.initialCapitalString$>:)
    @NSManaged public func addToMessages(_ values: NSSet)

    @objc(remove<$Relationship.name.initialCapitalString$>:)
    @NSManaged public func removeFrom<$Relationship.name.initialCapitalString$>(_ values: NSSet)
}
<$endif$><$endforeach do$>
