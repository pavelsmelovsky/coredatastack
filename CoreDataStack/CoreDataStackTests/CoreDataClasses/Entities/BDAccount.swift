import Foundation
import CoreData

@objc(BDAccount)
public class BDAccount: NSManagedObject {
    // MARK: - Entity name

    override open class var entityName: String {
        return "Account"
    }

    // MARK: - Attributes and relationships definitions

    struct Attributes {
        static let billing = "billing"
        static let createdAt = "createdAt"
        static let identifier = "identifier"
        static let number = "number"
    }

    struct Relationships {
        static let user = "user"
    }
}
