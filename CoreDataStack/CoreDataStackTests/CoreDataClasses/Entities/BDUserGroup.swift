import Foundation
import CoreData
import CoreDataStack

@objc(BDUserGroup)
public class BDUserGroup: NSManagedObject {
    // MARK: - Entity name

    override open class var entityName: String {
        return "UserGroup"
    }

    // MARK: - Attributes and relationships definitions

    struct Attributes {
        static let identifier = "identifier"
        static let name = "name"
    }

    struct Relationships {
        static let users = "users"
    }
}
