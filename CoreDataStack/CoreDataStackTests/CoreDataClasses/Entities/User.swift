import Foundation
import CoreData

@objc(User)
public class User: NSManagedObject {
    // MARK: - Entity name

    override open class var entityName: String {
        return "User"
    }

    // MARK: - Attributes and relationships definitions

    struct Attributes {
        static let age = "age"
        static let email = "email"
        static let identifier = "identifier"
        static let name = "name"
    }

    struct Relationships {
        static let accounts = "accounts"
        static let groups = "groups"
    }
}
