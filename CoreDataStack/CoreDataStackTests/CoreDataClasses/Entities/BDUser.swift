import Foundation
import CoreData

@objc(BDUser)
public class BDUser: NSManagedObject {
    // MARK: - Entity name

    override open class var entityName: String {
        return "User"
    }

    @objc(importEmail:)
    func importEmail(_ object: Any) -> Bool {
        guard let object = object as? [String: Any] else { return false }
        guard let email = object["email"] as? String else { return false }

        self.email = email
        return true
    }

    // MARK: - Attributes and relationships definitions

    struct Attributes {
        static let age = "age"
        static let email = "email"
        static let identifier = "identifier"
        static let name = "name"
    }

    struct Relationships {
        static let accounts = "accounts"
        static let groups = "groups"
    }
}
