#!/bin/sh

mogenerator \
--template-path=. \
--model CoreDataStack.xcdatamodeld \
--output-dir ./Entities \
--swift \
--template-var scalarsWhenNonOptional=true \
--machine-dir ./Entities/BaseEntities \
--human-dir ./Entities

mogenerator \
--template-path=. \
--model BigDataModel.xcdatamodeld \
--output-dir ./Entities \
--swift \
--template-var scalarsWhenNonOptional=true \
--machine-dir ./Entities/BaseEntities \
--human-dir ./Entities
