//
//  TestSyncModel.swift
//  CoreDataStackTests
//
//  Created by Pavel Smelovsky on 25/12/2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import XCTest
import CoreData
@testable import CoreDataStack

class TestSyncModel: XCTestCase {
    var bundle: Bundle!

    var stack: CoreDataStackSubclass!

    var json1: JSONObjectArray!
    var json2: [JSONObjectArray]!
    var json2result: JSONObjectArray!

    let dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    let dateFormatter = DateFormatter()

    override func setUp() {
        self.bundle = Bundle(for: type(of: self))

        self.json1 = self.loadJson("syncmodel")
        self.json2 = self.loadJson("syncmodel-new")
        self.json2result = self.loadJson("syncmodel-new-result")

        self.stack = PersistentContainerStack(name: "SyncModel", bundle: self.bundle)

        self.stack.automaticallyMergesChangesToViewContext = true

        self.dateFormatter.dateFormat = self.dateFormat
        let importer = CoreDataImporter(json: json1, type: SyncEntity.self, dateFormatter: self.dateFormatter, context: self.stack.viewContext)
        importer.parse()
        self.save(context: importer.context)
    }

    override func tearDown() {
        do {
            try self.stack.destroyStores()
        } catch let e {
            fatalError(e.localizedDescription)
        }
    }

    func testImport() {
        let sort = Sort(keyPath: "createdAt", ascending: false)
        let entities = self.stack.viewContext.findAll(of: SyncEntity.self, with: nil, sortedBy: [sort])

        compareEntities(entities, toJson: self.json1)

        // sync new data

        var fromDate: Date = Date()
        for json in self.json2 {
            let sync = CoreDataSyncer(json: json, type: SyncEntity.self, sortPropertyType: Date.self, dateFormatter: nil, context: self.stack.viewContext)

            sync.sync(from: fromDate)
            self.save(context: sync.context)

            guard let dateString = json.last?["createdAt"] as? String else { fatalError() }
            fromDate = self.dateFormatter.date(from: dateString)!
        }

        let entities2 = self.stack.viewContext.findAll(of: SyncEntity.self, with: nil, sortedBy: [sort])
        compareEntities(entities2, toJson: self.json2result)
    }

    private func compareEntities(_ entities: [SyncEntity], toJson json: JSONObjectArray) {
        XCTAssertEqual(entities.count, json.count)

        for counter in 0 ..< min(entities.count, json.count) {
            let entity = entities[counter]
            let jsonItem = json[counter]

            XCTAssertEqual(entity.number, jsonItem["number"] as? Int32)
            XCTAssertEqual(entity.idx, jsonItem["id"] as? String)

            let dateString = jsonItem["createdAt"] as? String
            XCTAssertNotNil(dateString)

            if let dateString = dateString {
                let date = self.dateFormatter.date(from: dateString)
                XCTAssertEqual(entity.createdAt, date)
            }
        }
    }

    private func loadJson<T>(_ filename: String) -> T {
        let file = self.bundle.url(forResource: filename, withExtension: "json")!
        do {
            let data = try Data(contentsOf: file)
            guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? T else {
                fatalError()
            }
            return json
        } catch {
            fatalError(error.localizedDescription)
        }
    }

    private func save(context: NSManagedObjectContext) {
        do {
            try context.saveToPersistentStoreAndWait()
        } catch {
            fatalError(error.localizedDescription)
        }
    }
}
