//
//  SyncEntity.swift
//  CoreDataStackTests
//
//  Created by Pavel Smelovsky on 26/12/2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import UIKit
import CoreData
import CoreDataStack

@objc(SyncEntity)
class SyncEntity: NSManagedObject {
    override open class var entityName: String {
        return "SyncEntity"
    }
}
