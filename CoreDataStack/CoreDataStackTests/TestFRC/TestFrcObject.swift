//
//  TestFrcObject.swift
//  CoreDataStackTests
//
//  Created by Pavel Smelovsky on 16.09.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import CoreData
@testable import CoreDataStack

@objc(TestFrcObject)
class TestFrcObject: NSManagedObject {
    override class var entityName: String {
        return "Object"
    }
}
