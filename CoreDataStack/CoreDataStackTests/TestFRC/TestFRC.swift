//
//  TestFRC.swift
//  CoreDataStackTests
//
//  Created by Pavel Smelovsky on 16.09.2018.
//  Copyright © 2018 Smelovsky.Me. All rights reserved.
//

import XCTest
@testable import CoreDataStack
import CoreData

class TestFRC: XCTestCase, NSFetchedResultsControllerDelegate {
    var stack: CoreDataStack!

    override func setUp() {
        super.setUp()

        stack = CoreDataStack(name: "TestFRC", url: nil, bundle: Bundle(for: TestFRC.self))

        var index = 0
        while index < 100 {
            let obj = TestFrcObject(context: stack.viewContext)
            obj.name = "Object \(index)"
            obj.order = Int64(index)

            index += 5
        }

        self.save(context: stack.viewContext)
    }
    
    override func tearDown() {
        TestFrcObject.truncateAll(in: stack.viewContext)
        self.save(context: stack.viewContext)

        super.tearDown()
    }

    var testUpdatesExpectation: XCTestExpectation?

    func testUpdates() {
        testUpdatesExpectation = self.expectation(description: "Test updates")

        let request: NSFetchRequest<TestFrcObject> = TestFrcObject.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "order", ascending: true)]

        let frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: stack.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        do {
            try frc.performFetch()
        } catch {
            fatalError("Cannot perform objects: \(error.localizedDescription)")
        }

        print(objects: frc.fetchedObjects!)

        debugPrint()
        debugPrint()
        debugPrint("Update objects")

        var objects = frc.fetchedObjects!

        createObject(name: "Object -1", order: -1)
        createObject(name: "Object 88", order: 88)
        delete(object: objects[1]) // delete with order 1 * 5

        createObject(name: "Object 51", order: 51)
        delete(object: objects[10])
        createObject(name: "Object 49", order: 49)

        createObject(name: "Object 12", order: 12)
        createObject(name: "Object 17", order: 17)

        objects[8].order = 7 * 5 - 1 // less then 7 element
        objects[12].order = 13 * 5 + 1 // greater then 13 element

        objects[16].order = 16 * 5 + 1 // dont move, only increase order
        objects[16].name = "Object \(16 * 5 + 1) updated"

        objects[18].order = 18 * 5 + 10
        objects[18].name = "Object \(18 * 5 + 10) updated"

        delete(object: objects[11])

        objects.last?.order += 1
        objects.last?.name = "Object \(objects.last!.order) updated"

        save(context: stack.viewContext)

        debugPrint()
        debugPrint()
        print(objects: frc.fetchedObjects!)

        wait(for: [testUpdatesExpectation!], timeout: 5)
    }

    private func save(context: NSManagedObjectContext, line: Int = #line) {
        do {
            try context.saveToPersistentStoreAndWait()
        } catch {
            fatalError("Saving error at line: \(line)\n \(error.localizedDescription)")
        }
    }

    @discardableResult
    private func createObject(name: String, order: Int64) -> TestFrcObject {
        let obj = TestFrcObject(context: stack.viewContext)
        obj.name = name
        obj.order = order
        return obj
    }

    private func delete(object: TestFrcObject) {
        stack.viewContext.delete(object)
    }

    private func print(objects: [TestFrcObject]) {
        var index = 0
        for object in objects {
            debugPrint("index: \(index), name: \(object.name!)")
            index += 1
        }
    }

    // MARK: - FRC delegate

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        guard let object = anObject as? TestFrcObject else { fatalError() }

        switch type {
        case .insert:
            debugPrint("Insert \(object.name!) at \(newIndexPath!.row)")
        case .update:
            debugPrint("Update \(object.name!) at \(indexPath!.row)")
        case .move:
            debugPrint("Move \(object.name!) from \(indexPath!.row) to \(newIndexPath!.row)")
        case .delete:
            debugPrint("Delete \(object.name!) at \(indexPath!.row)")
        }
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        testUpdatesExpectation?.fulfill()
    }
}
