//
//  TestCoreStackImport.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 12.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import XCTest
import Foundation
import UIKit
import CoreData

@testable import CoreDataStack

class TestCoreStackImport: XCTestCase, NSFetchedResultsControllerDelegate {
    var bundle: Bundle!

    var stack: CoreDataStackSubclass!

    var frc: NSFetchedResultsController<User>!

    var json1: JSONObjectArray!
    var json2: JSONObjectArray!

    override func setUp() {
        super.setUp()

        self.bundle = Bundle(for: type(of: self))

        self.stack = PersistentContainerStack(name: "CoreDataStack", bundle: self.bundle)
        self.stack.automaticallyMergesChangesToViewContext = true

        let import1 = self.bundle.url(forResource: "import1", withExtension: "json")!
        do {
            let data1 = try Data(contentsOf: import1)
            guard let json1 = try JSONSerialization.jsonObject(with: data1, options: []) as? JSONObjectArray else {
                fatalError()
            }
            self.json1 = json1

            let import2 = self.bundle.url(forResource: "import2", withExtension: "json")!
            let data2 = try Data(contentsOf: import2)
            guard let json2 = try JSONSerialization.jsonObject(with: data2, options: []) as? JSONObjectArray else {
                fatalError()
            }
            self.json2 = json2
        } catch let error {
            XCTFail(error.localizedDescription)
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()

        self.stack = nil
        self.frc = nil
    }

    weak var observer: NSObjectProtocol!
    func testImport() {
        let exp = expectation(description: "testImport")

        let context = self.stack.viewContext!

        User.truncateAll(in: context)
        try? context.saveToPersistentStoreAndWait()

        let didChangeContext = XCTNSNotificationExpectation(name: .NSManagedObjectContextObjectsDidChange, object: context)
        didChangeContext.handler = { notification in
            guard let ctx = notification.object as? NSManagedObjectContext else {
                return false
            }
            print("Context \(ctx.name ?? "<unknown>") did change")

            return true
        }

        DispatchQueue.global().async {
            print("Import 1 start")
            self._import(self.json1)

            self._testGroups1(in: self.stack.viewContext)

            print("Import 2 start")
            self._import(self.json2)

            self._testGroups2(in: self.stack.viewContext)

            let count = UserGroup.count(in: self.stack.viewContext)
            XCTAssertEqual(count, 5) // must be equal to 4, but now Importer does not remove zombies objects

            print("Import completed")
            exp.fulfill()
        }

        wait(for: [exp, didChangeContext], timeout: 2000)
    }

    private func _import(_ json: JSONObjectArray) {
        let importContext = self.stack.newBackgroundContext(name: "ImportContext")

        importContext.performAndWait {
            let dateformatter = DateFormatter()
            let importer = CoreDataImporter(json: json, type: User.self, dateFormatter: dateformatter, context: importContext)
            let importedObjects = importer.parse()
            XCTAssertEqual(importedObjects.count, 2)

            do {
                try importContext.saveToPersistentStoreAndWait()
            } catch let error {
                XCTFail(error.localizedDescription)
            }

            self._compareJson(json, with: importContext)

            self.stack.viewContext.performAndWait {
                self._compareJson(json, with: self.stack.viewContext)
            }
        }

        print("Leave import function")
    }

    private func _compareJson(_ json: JSONObjectArray, with context: NSManagedObjectContext) {
        print("Compare json to context: \(context.name ?? "<unknown>")")

        let json = json.sorted { (left, right) -> Bool in
            guard let lId = left["id"] as? Int,
                let rId = right["id"] as? Int else {
                    fatalError()
            }
            return lId < rId
        }

        let sortById = Sort(keyPath: User.Attributes.identifier, ascending: true)
        let objects = context.findAll(of: User.self, with: nil, sortedBy: [sortById], page: nil)

        var jsonIter = json.makeIterator()
        var objIter = objects.makeIterator()

        var attrs: JSONObject! = jsonIter.next()
        var obj: User! = objIter.next()

        while true {
            if attrs == nil || obj == nil {
                XCTAssertNotNil(attrs)
                XCTAssertNotNil(obj)

                break
            }

            guard
                let id = attrs["id"] as? Int,
                let name = attrs["name"] as? String,
                let email = attrs["email"] as? String,
                let age = attrs["age"] as? Int else {
                    fatalError()
            }

            XCTAssertEqual(Int(obj.identifier), id)
            XCTAssertEqual(obj.name, name)
            XCTAssertEqual(obj.email, email)
            XCTAssertEqual(Int(obj.age), age)

            let predicate = NSPredicate(format: "%K == %@", Account.Relationships.user, obj)
            let accounts = context.findAll(of: Account.self, with: predicate, sortedBy: [sortById], page: nil)

            guard let userAccounts = attrs["userAccounts"] as? JSONObjectArray else {
                fatalError()
            }
            self.compareAccounts(accounts, with: userAccounts)

            obj = objIter.next()
            attrs = jsonIter.next()

            if obj == nil && attrs == nil {
                break
            }
        }

        let predicate = NSPredicate(format: "user = nil")
        let accCount = Account.count(predicate: predicate, in: context)
        XCTAssertEqual(accCount, 0)
    }

    func compareAccounts(_ accounts: [Account], with json: JSONObjectArray) {
        var accIter = accounts.makeIterator()
        var jsonIter = json.makeIterator()

        var acc: Account! = accIter.next()
        var attrs: JSONObject! = jsonIter.next()

        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"

        while true {
            if acc == nil || attrs == nil {
                XCTAssertNotNil(acc)
                XCTAssertNotNil(attrs)

                break
            }

            guard
                let id = attrs["id"] as? Int,
                let createdAtString = attrs["createdAt"] as? String,
                let number = attrs["number"] as? String,
                let billing = attrs["billing"] as? Double else {
                fatalError()
            }

            let createdAt = dateFormat.date(from: createdAtString)

            XCTAssertEqual(Int(acc.identifier), id)
            XCTAssertEqual(acc.createdAt, createdAt)
            XCTAssertEqual(acc.number, number)
            XCTAssertEqual(acc.billing, billing)

            acc = accIter.next()
            attrs = jsonIter.next()

            if acc == nil && attrs == nil {
                break
            }
        }
    }

    private func _testGroups1(in context: NSManagedObjectContext) {
        let predicate1 = NSPredicate(format: "identifier = 1")
        let userGroup1 = UserGroup.findFirst(predicate: predicate1, in: context)
        XCTAssertNotNil(userGroup1)
        if let userGroup = userGroup1 {
            XCTAssertEqual(userGroup.users?.count, 2)
        }

        let predicate2 = NSPredicate(format: "identifier = 2")
        let userGroup2 = UserGroup.findFirst(predicate: predicate2, in: context)
        XCTAssertNotNil(userGroup2)
        if let userGroup = userGroup2 {
            XCTAssertEqual(userGroup.users?.count, 1)
        }

        let predicate3 = NSPredicate(format: "identifier = 3")
        let userGroup3 = UserGroup.findFirst(predicate: predicate3, in: context)
        XCTAssertNotNil(userGroup3)
        if let userGroup = userGroup3 {
            XCTAssertEqual(userGroup.users?.count, 1)
        }
    }

    private func _testGroups2(in context: NSManagedObjectContext) {
        let predicate1 = NSPredicate(format: "identifier = 1")
        let userGroup1 = UserGroup.findFirst(predicate: predicate1, in: context)
        XCTAssertNotNil(userGroup1)
        if let userGroup = userGroup1 {
            XCTAssertEqual(userGroup.users?.count, 2)
        }

        let predicate2 = NSPredicate(format: "identifier = 2")
        let userGroup2 = UserGroup.findFirst(predicate: predicate2, in: context)
        XCTAssertNotNil(userGroup2)
        if let userGroup = userGroup2 {
            XCTAssertEqual(userGroup.users?.count, 2)
            print("")
        }

        let predicate4 = NSPredicate(format: "identifier = 4")
        let userGroup4 = UserGroup.findFirst(predicate: predicate4, in: context)
        XCTAssertNotNil(userGroup4)
        if let userGroup = userGroup4 {
            XCTAssertEqual(userGroup.users?.count, 1)
        }

        let predicate5 = NSPredicate(format: "identifier = 5")
        let userGroup5 = UserGroup.findFirst(predicate: predicate5, in: context)
        XCTAssertNotNil(userGroup5)
        if let userGroup = userGroup5 {
            XCTAssertEqual(userGroup.users?.count, 1)
        }
    }

    // MARK: - FRC delegate

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        print("controllerWillChangeContent")
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        print("controllerDidChangeContent")
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        print("didChange:atSectionIndex")
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        print("didChange:at:for:newIndex")
    }
}
