//
//  RSCoreDataTest.swift
//  Roistat
//
//  Created by Pavel Smelovsky on 17.04.16.
//  Copyright © 2016 Roistat. All rights reserved.
//

import XCTest

@testable import PSCoreDataStack

class RSCoreDataTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.

    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testDataParsing() {
        createProjects()
        createAuthorizedUsers()
        getData()
    }

    func createProjects() {
        let jsonString = "{\"projects\":[{\"id\":13540,\"name\":\"Demo\",\"profit\":null,\"creation_date\":\"2016-03-12 10:18:31\",\"currency\":\"RUB\",\"is_owner\":1},{\"id\":13783,\"name\":\"4Демо\",\"profit\":null,\"creation_date\":\"2016-03-17 13:13:05\",\"currency\":\"RUB\",\"is_owner\":1},{\"id\":12382,\"name\":\"pbx3test\",\"profit\":\"0\",\"creation_date\":\"2016-02-15 12:58:32\",\"currency\":\"RUB\",\"is_owner\":0}],\"status\":\"success\"}"
        let jsonData = jsonString.data(using: String.Encoding.utf8)
        guard let data = try? JSONSerialization.jsonObject(with: jsonData!, options: []) as? [String: AnyObject] else {
            fatalError()
        }

        guard let jsonProjects = data["projects"] as? JSONObjectArray else {
            fatalError()
        }

        var projects: [RSProject]?

        CoreDataStack.saveAndWait { (context) in
            projects = RSProject.importObjects(jsonProjects, in: context) as? [RSProject]
        }

        CoreDataStack.viewContext.saveToPersistentStore()
        XCTAssertNotNil(projects)
    }

    func createAuthorizedUsers() {
        let jsonString = "{\"authorized_users\":[{\"email\":\"pavel.smelovsky@gmail.com\",\"name\":\"Павел\",\"access\":\"owner\",\"project\":{\"id\":13540}},{\"email\":\"_@lkb.me\",\"name\":null,\"access\":\"read\",\"project\":{\"id\":12382}}],\"status\":\"success\"}"
        let jsonData = jsonString.data(using: String.Encoding.utf8)
        guard let data = try? JSONSerialization.jsonObject(with: jsonData!, options: []) as? [String: AnyObject] else {
            fatalError()
        }

        guard let jsonUsers = data["authorized_users"] as? JSONObjectArray else {
            fatalError()
        }

        var authorizedUsers: [RSAuthorizedUser]?

        CoreDataStack.saveAndWait { (context) in
            authorizedUsers = RSAuthorizedUser.importObjects(jsonUsers, in: context) as? [RSAuthorizedUser]
        }

        XCTAssertNotNil(authorizedUsers)
    }

    func getData() {
        guard let projects = RSProject.findAll() as [RSProject] else {
            fatalError()
        }
        for project in projects {
            let users = project.users
            print(users.description)
        }

        XCTAssertNotNil(projects)
    }
}
