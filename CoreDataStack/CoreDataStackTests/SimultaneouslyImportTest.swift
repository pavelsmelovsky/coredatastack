//
//  SimultaneouslyImportTest.swift
//  CoreDataStackTests
//
//  Created by Pavel Smelovsky on 12.12.2017.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import XCTest
@testable import CoreDataStack
import CoreData

class SimultaneouslyImportTest: XCTestCase {
    var stack: CoreDataStackSubclass!
    var bundle = Bundle(for: SimultaneouslyImportTest.self)

    var data: JSONObjectArray!

    override func setUp() {
        super.setUp()

        self.stack = PersistentContainerStack(name: "BigDataModel", bundle: bundle)

        let jsonfile = bundle.url(forResource: "bigdata", withExtension: "json")!
        guard let data = try? Data(contentsOf: jsonfile),
            let obj = try? JSONSerialization.jsonObject(with: data, options: []) as? JSONObjectArray else {
                fatalError()
        }
        self.data = obj
    }

    override func tearDown() {
        do {
            try self.stack.destroyStores()
            self.stack = nil

            let url = NSPersistentStore.storeUrl(withName: "BigDataModel", bundle: bundle)
            try FileManager.default.removeItem(at: url)
        } catch let error {
            XCTFail(error.localizedDescription)
        }

        super.tearDown()
    }

    func testSingleContextSwitching() {
        self.stack.useSingleSaveContext = true
        XCTAssertNotNil(self.stack.importContext)

        self.stack.useSingleSaveContext = false
        XCTAssertNil(self.stack.importContext)
    }

    func testSynchroneouslyImport() {
        let exp = expectation(description: "testSinchroneouslyImport")
        exp.assertForOverFulfill = true

        self.stack.useSingleSaveContext = false

        _import(objects: self.data, of: BDUser.self) { (success, error) in
            XCTAssertNil(error)
            XCTAssertTrue(success)

            let count = BDUser.count(in: self.stack.viewContext)
            print("Count 1: \(count)")
            XCTAssertEqual(count, UInt(self.data.count))

            let accCount = BDAccount.count(in: self.stack.viewContext)
            XCTAssertEqual(accCount, UInt(self.data.count * 4))

            // Sync again:

            var idx = 0
            let data = self.data.filter({ _ -> Bool in
                let ret = idx % 2 == 0
                idx += 1
                return ret
            })

            self._sync(objects: data, of: BDUser.self) { (success, error) in
                XCTAssertNil(error)
                XCTAssertTrue(success)

                let count = BDUser.count(in: self.stack.viewContext)
                print("Count 2: \(count)")
                XCTAssertEqual(count, UInt(self.data.count / 2))

                let accCount = BDAccount.count(in: self.stack.viewContext)
                XCTAssertEqual(accCount, UInt((self.data.count * 4) / 2))

                exp.fulfill()
            }
        }

        waitForExpectations(timeout: 480) { (error) in
            XCTAssertNil(error)
        }
    }

    private func _import<T: NSManagedObject>(objects: JSONObjectArray, of type: T.Type, delay: Int? = nil, completion: @escaping (Bool, Error?) -> Swift.Void) {
        let block = {
            self.stack.save({ (context) in
                let importer = CoreDataImporter(json: objects, type: type, dateFormatter: DateFormatter(), context: context)
                importer.parse()

                debugPrint("inserted: \(context.insertedObjects.count)")
                debugPrint("updated: \(context.updatedObjects.count)")
                debugPrint("deleted: \(context.deletedObjects.count)")
            }, completion: completion)
        }
        if let delay = delay {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(delay), execute: block)
        } else {
            block()
        }
    }

    private func _sync<T: NSManagedObject>(objects: JSONObjectArray, of type: T.Type, completion: @escaping (Bool, Error?) -> Swift.Void) {
        self.stack.save({ (context) in
            let importer = CoreDataImporter(json: objects, type: type, dateFormatter: DateFormatter(), context: context)
            importer.parse()
            importer.delete()

            debugPrint("inserted: \(context.insertedObjects.count)")
            debugPrint("updated: \(context.updatedObjects.count)")
            debugPrint("deleted: \(context.deletedObjects.count)")
        }, completion: completion)
    }

    func testAsynchroneouslyImport() {
        let exp = expectation(description: "testAsynchroneouslyImport")
        exp.assertForOverFulfill = true
        exp.expectedFulfillmentCount = 2

        self.stack.useSingleSaveContext = true

        _import(objects: self.data, of: BDUser.self) { (success, error) in
            XCTAssertNil(error)
            XCTAssertTrue(success)

            let count = BDUser.count(in: self.stack.viewContext)
            print("Count 1: \(count)")
            XCTAssertEqual(count, UInt(self.data.count))

            let accCount = BDAccount.count(in: self.stack.viewContext)
            XCTAssertEqual(accCount, UInt(self.data.count * 4))

            exp.fulfill()
        }

        self._import(objects: data, of: BDUser.self, delay: 300) { (success, error) in
            XCTAssertNil(error)
            XCTAssertTrue(success) // falst because data should be already imported in previous import request

            let count = BDUser.count(in: self.stack.viewContext)
            print("Count 2: \(count)")
            XCTAssertEqual(count, UInt(self.data.count))

            let accCount = BDAccount.count(in: self.stack.viewContext)
            XCTAssertEqual(accCount, UInt((self.data.count * 4)))

            exp.fulfill()
        }

        waitForExpectations(timeout: 480) { (error) in
            XCTAssertNil(error)
        }
    }

    func testImport() {
        let exp = expectation(description: "simultaneouslyImport")
        exp.assertForOverFulfill = true

        self.stack.save({ (context) in
            let importer = CoreDataImporter(json: self.data, type: BDUser.self, dateFormatter: DateFormatter(), context: context)
            importer.parse()
        }, completion: { success, error in
            XCTAssertNil(error)
            XCTAssertTrue(success)

            let count = BDUser.count(in: self.stack.viewContext)
            XCTAssertEqual(count, UInt(self.data.count))

            let accCount = BDAccount.count(in: self.stack.viewContext)
            XCTAssertEqual(accCount, 4000)

            exp.fulfill()
        })

        waitForExpectations(timeout: 60) { (error) in
            XCTAssertNil(error)
        }
    }

    func testDifferentContexts() {
        var user: BDUser! = nil

        let ctx = self.stack.newBackgroundContext(name: "testBackgroundContext")
        ctx.performAndWait {
            guard let entity = NSEntityDescription.entity(forEntityName: BDUser.entityName, in: ctx) else { fatalError() }

            user = BDUser(entity: entity, insertInto: ctx)
            user.identifier = "uuid"
            user.name = "Pavel"
            user.age = 333
            user.email = "pavel@Smelovsky.Me"

            do {
                try ctx.saveToPersistentStoreAndWait()
            } catch let error {
                XCTFail(error.localizedDescription)
            }
        }

        let localUser = user.inContext(self.stack.viewContext)
        XCTAssertNotNil(localUser)
        XCTAssertEqual(localUser?.identifier, "uuid")
    }

//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            let ctx = self.stack.viewContext!
//            ctx.importObjects(self.data, of: User.self)
//        }
//    }

}
