//
//  TestCoreDataStack.swift
//  CoreDataStack
//
//  Created by Pavel Smelovsky on 09.03.17.
//  Copyright © 2017 Smelovsky.Me. All rights reserved.
//

import XCTest

@testable import CoreDataStack
import CoreData

class TestCoreDataStack: XCTestCase {
    var stack: CoreDataStackSubclass!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.

        let bundle = Bundle(for: type(of: self))
        self.stack = PersistentContainerStack(name: "CoreDataStack", bundle: bundle)

        self.stack.automaticallyMergesChangesToViewContext = true
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testCDS() {
        let moc = self.stack.viewContext!

        User.truncateAll(in: moc)
        Account.truncateAll(in: moc)
        UserGroup.truncateAll(in: moc)
        try? moc.saveToPersistentStoreAndWait()

        let user = createUser(in: moc)

        var accounts = [Account]()
        for idx in 10..<13 {
            let account = createAccount(id: idx, for: user, in: moc)
            accounts.append(account)
        }

        do {
            try moc.saveToPersistentStoreAndWait()
        } catch let error {
            XCTFail("Error saving context \(error.localizedDescription)")
        }

        let exp = XCTNSNotificationExpectation(name: .NSManagedObjectContextObjectsDidChange, object: moc)
        exp.handler = { notification in
            let name = user.name
            XCTAssertEqual(name, "Pavel Smelovsky")

            return self.on(notification: notification)
        }

        let backgroundContext = self.stack.newBackgroundContext(name: "Another context")
        let backgroundContextNotification = XCTNSNotificationExpectation(name: .NSManagedObjectContextObjectsDidChange, object: backgroundContext)
        backgroundContextNotification.handler = { notification in
            return self.on(notification: notification)
        }
        self.compare(user: user, accounts: accounts, in: backgroundContext)

        wait(for: [exp, backgroundContextNotification], timeout: 5)
//        waitForExpectations(timeout: 5) { (error) in
//            XCTAssertNil(error)
//        }
    }

    private func createUser(in context: NSManagedObjectContext) -> User {
        guard let entity = NSEntityDescription.entity(forEntityName: User.entityName, in: context) else { fatalError() }

        let user = User(entity: entity, insertInto: context)

        user.identifier = 1
        user.name = "Pavel"
        user.email = "pavel.smelovsky@gmail.com"
        user.age = 30

        return user
    }

    private func createAccount(id: Int, for user: User, in context: NSManagedObjectContext) -> Account {
        guard let entity = NSEntityDescription.entity(forEntityName: Account.entityName, in: context) else { fatalError() }

        let acc = Account(entity: entity, insertInto: context)

        acc.identifier = Int64(id)
        acc.number = UUID().uuidString
        acc.createdAt = Date()
        acc.user = user

        return acc
    }

    private func compare(user: User, accounts: [Account], in context: NSManagedObjectContext) {
        context.perform {
            let localUser: User! = user.inContext(context)
            XCTAssertNotNil(localUser)
            XCTAssertEqual(localUser.objectID, user.objectID)

            for account in accounts {
                let localAccount = account.inContext(context)
                XCTAssertNotNil(localAccount)
            }

            localUser.name = "Pavel Smelovsky"
            do {
                try context.saveToPersistentStoreAndWait()
            } catch let error {
                XCTFail("Error saving context \(error.localizedDescription)")
            }
        }
    }

    private func on(notification: Notification) -> Bool {
        guard let ctx = notification.object as? NSManagedObjectContext else {
            return false
        }
        debugPrint("NSManagedObjectContextObjectsDidChange for context \(ctx.name ?? "<unknown>")")
//            guard let userInfo = notification.userInfo else {
//                fatalError()
//            }
//
//            debugPrint(">>>>>>>>>>> KEYS")
//            for item in userInfo {
//                debugPrint("\(item.key):")
//                if let objects = item.value as? Set<NSManagedObject> {
//                    debugPrint(objects)
//                } else if let ctx = item.value as? NSManagedObjectContext {
//                    debugPrint("Context: \(String(describing: ctx.name))")
//                }
//            }
//            debugPrint("KEYS <<<<<<<<<<<")
        return true
    }

//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }

}
